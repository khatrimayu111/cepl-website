#!/bin/bash

# Check helm chart is installed or create
# reuse installed values and resets data

export CHECK_WEB=$(helm ls -q cepl-web --tiller-namespace cepl)
if [ "$CHECK_WEB" = "cepl-web" ]
then
    echo "Updating existing cepl-web . . ."
    helm upgrade cepl-web \
        --tiller-namespace cepl \
        --namespace cepl \
        --reuse-values \
        helm-chart/cepl-web
fi
